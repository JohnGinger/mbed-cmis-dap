/* CMSIS-DAP Interface Firmware
 * Copyright (c) 2009-2013 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef TARGET_FLASH_H
#define TARGET_FLASH_H

#include "target_struct.h"
#include "swd_host.h"
#include <stdint.h>

#define FLASH_SECTOR_SIZE           (1024)

#define TARGET_AUTO_INCREMENT_PAGE_SIZE    0x1000

static uint8_t target_flash_init(uint32_t clk);
static uint8_t target_flash_uninit(void);
static uint8_t target_flash_erase_chip(void);
static uint8_t target_flash_erase_sector(uint32_t adr);
static uint8_t target_flash_program_page(uint32_t adr, uint8_t * buf, uint32_t size);

static const uint32_t LPC11U24_FLM[] = {
    0xE00ABE00, 0x062D780D, 0x24084068, 0xD3000040, 0x1E644058, 0x1C49D1FA, 0x2A001E52, 0x4770D1F2,
    0x28100b00, 0x08c0d301, 0x4770300e, 0x4863b530, 0x22fd7801, 0x70014011, 0x730323aa, 0x73042455, 
    0x08497801, 0x70010049, 0x73047303, 0x6a0a495c, 0x432a2520, 0x6a0a620a, 0xd5fc0652, 0x21014d57, 
    0x73293580, 0x60422213, 0x73047303, 0x430a7802, 0x73037002, 0x68827304, 0xd5fc0152, 0x712a2207, 
    0x722a2209, 0x25027802, 0x7002432a, 0x73047303, 0x3840484a, 0x494c7001, 0x4449484a, 0x20006008, 
    0x2000bd30, 0xb5f84770, 0x20324c48, 0x2500444c, 0x46222607, 0x4621c261, 0x4f453114, 0x91004620, 
    0x696047b8, 0xd10c2800, 0x46212034, 0x483ec161, 0x68004448, 0x462060e0, 0x47b89900, 0x28006960, 
    0x2001d000, 0xb5f8bdf8, 0xff9af7ff, 0x46044d37, 0x2032444d, 0x4629606c, 0x311460ac, 0x4e346028, 
    0x4628460f, 0x696847b0, 0xd10d2800, 0x2034606c, 0x602860ac, 0x4639482c, 0x68004448, 0x462860e8, 
    0x696847b0, 0xd0002800, 0xbdf82001, 0x4614b5f8, 0xd11e0006, 0x0180200b, 0x6bc11820, 0x42814825, 
    0x4825d03a, 0xd0374281, 0x42814824, 0x4824d034, 0xd0314281, 0x68206861, 0x184068e2, 0x188968a1, 
    0x69211840, 0x69611840, 0x69a11840, 0x42401840, 0x463061e0, 0xff54f7ff, 0x21324d14, 0x6068444d, 
    0x60a86029, 0x31144629, 0x46284f11, 0x47b89100, 0x28006968, 0x606ed110, 0x60ac2033, 0x20016028, 
    0x60e80240, 0x44484808, 0x61286800, 0x99004628, 0x696847b8, 0xd0002800, 0xbdf82001, 0x400fc080, 
    0x400fc180, 0x0000ea60, 0x00000004, 0x00000008, 0x1fff1ff1, 0x4e697370, 0x12345678, 0x87654321, 
    0x43218765, 0x00000000, 0x00000000, 
};

static const TARGET_FLASH flash = {
    0x1000002D, // Init
    0x100000A3, // UnInit
    0x100000A7, // EraseChip
    0x100000E7, // EraseSector
    0x1000012D, // ProgramPage
    //breakpoint: breakpoint (RAM start + 1)
    // RSB : static_base (RAM start + size of the flash algorithm)
    // RSP : Initial stack pointer - seems standard to put this at the 4KB boundary if there are more than 4kB of RAM
    {0x10000001, 0x10000000+sizeof(LPC11U24_FLM), 0x10001000}, // {breakpoint, RSB, RSP}

    0x10000500, // program_buffer
    0x10000000, // algo_start
    sizeof(LPC11U24_FLM), // algo_size
    LPC11U24_FLM,// image
    256         // ram_to_flash_bytes_to_be_written
};

static uint8_t target_flash_init(uint32_t clk) {
    // Download flash programming algorithm to target and initialise.
    if (!swd_write_memory(flash.algo_start, (uint8_t *)flash.image, flash.algo_size)) {
        return 0;
    }

    if (!swd_flash_syscall_exec(&flash.sys_call_param, flash.init, 0, 0 /* clk value is not used */, 0, 0)) {
        return 0;
    }

    return 1;
}

static uint8_t target_flash_erase_sector(unsigned int sector) {
    if (!swd_flash_syscall_exec(&flash.sys_call_param, flash.erase_sector, sector*0x1000, 0, 0, 0)) {
        return 0;
    }

    return 1;
}

static uint8_t target_flash_erase_chip(void) {
    if (!swd_flash_syscall_exec(&flash.sys_call_param, flash.erase_chip, 0, 0, 0, 0)) {
        return 0;
    }

    return 1;
}

static uint8_t target_flash_program_page(uint32_t addr, uint8_t * buf, uint32_t size)
{
    uint32_t bytes_written = 0;

    // Program a page in target flash.
    if (!swd_write_memory(flash.program_buffer, buf, size)) {
        return 0;
    }

    while(bytes_written < size) {
        if (!swd_flash_syscall_exec(&flash.sys_call_param,
                                    flash.program_page,
                                    addr,
                                    flash.ram_to_flash_bytes_to_be_written,
                                    flash.program_buffer + bytes_written, 0)) {
            return 0;
        }

        bytes_written += flash.ram_to_flash_bytes_to_be_written;
        addr += flash.ram_to_flash_bytes_to_be_written;
    }

    return 1;
}


#endif
